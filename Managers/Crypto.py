from requests import Request, Session
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
import pandas as pd
import json

class Crypto:
    def __init__(self, API_TOKEN):
        self.API_TOKEN = API_TOKEN
    
    
    def fetchData(self):
        url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest'
        parameters = {
            'start':'1',
            'limit':'5000',
            'convert':'USD'
        }
        headers = {
            'Accepts': 'application/json',
            'X-CMC_PRO_API_KEY': self.API_TOKEN,
        }

        session = Session()
        session.headers.update(headers)

        try:
            response = session.get(url, params=parameters)
            data = json.loads(response.text)
            crypto_df = pd.DataFrame(data['data'])
            crypto_devices_df = crypto_df[["id", "name", "symbol", "slug"]]
            
            self.crypto_devices_df = crypto_devices_df
            return True
        except (ConnectionError, Timeout, TooManyRedirects) as e:
            print(e)
            self.crypto_devices_df = []
            return False