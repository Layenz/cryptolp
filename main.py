from Managers.Twitter import Twitter
from Managers.Crypto import Crypto
from Managers.Credential import Credential
from Managers.Config import Config
from AI.NLP.NLPManager import NLPManager

    
    
def initApp():
    """ Init Managers """
    config = Config()
    twitterManager = Twitter(Credential.getApiKeyTwitter())
    cryptoManager = Crypto(Credential.getApiKeyCoinMakert())
    nlpManager = NLPManager()
    
    cryptoManager.fetchData()
    nlpManager.generatePatternFromCrypto(cryptoManager.crypto_devices_df)
    print(cryptoManager.crypto_devices_df.value_counts())
    # nlpManager.createMatcherPattern()
    
    # """ Fetch Twitter Latest Tweet """
    # data = twitterManager.fetchData("Crypto", config.limitCallApi)
    # data = twitterManager.clearData(data)
    
    # analysis = nlpManager.analysisTweet(data)



if __name__ == '__main__':
    initApp()