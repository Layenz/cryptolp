import requests
import json
import pandas as pd

class Twitter:
    def __init__(self, API_TOKEN):
        self.API_TOKEN = API_TOKEN
    
    def search_twitter(self, query, tweet_fields):
        headers = {"Authorization": "Bearer {}".format(self.API_TOKEN)}

        url = "https://api.twitter.com/2/tweets/search/recent?query={}&{}".format(
            query, tweet_fields
        )
        response = requests.request("GET", url, headers=headers)

        print(response.status_code)

        if response.status_code != 200:
            raise Exception(response.status_code, response.text)
        return response.json()
    
    def fetchData(self, query, limit=1000):
        tweet_fields = "tweet.fields=text,author_id,created_at"
        json_response = self.search_twitter(query=query, tweet_fields=tweet_fields)
        encoded = json.dumps(json_response, indent=limit)
        decoded = json.loads(encoded)
        df = pd.DataFrame(decoded["data"])
        return df
    
    def clearData(self, data):
        df = data["text"].str.replace(r'[^a-zA-Z0-9€:.$#@ ]', '')
        return df