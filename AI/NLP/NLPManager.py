import spacy
import matplotlib.pyplot as plt
from spacy.matcher import Matcher

class NLPManager:
    def __init__(self):
        self.matcher_patterns = []
        self.nlp = spacy.load("en_core_web_sm")
        self.matcher = Matcher(self.nlp.vocab)
        
    
    def generatePatternFromCrypto(self, crypto): 
        for value in crypto['name']:
            if self.patternExist('CRYPTO_' + value.upper() + '_NAME_PATTERN') == False:
                self.matcher_patterns.append([
                    {'NAME': 'CRYPTO_' + value.upper() + '_NAME_PATTERN'},
                    {'PATTERN': [
                        {"TEXT": {"REGEX": value + "[S]?/gi"}}
                    ]}
                ])
        
        for value in crypto['symbol']:
            if self.patternExist('CRYPTO_' + value.upper() + '_SYMBOL_PATTERN') == False:
                self.matcher_patterns.append([
                    {'NAME': 'CRYPTO_' + value.upper() + '_SYMBOL_PATTERN'},
                    {'PATTERN': [
                        {"TEXT": {"REGEX": value + "[S]?/gi"}}
                    ]}
                ])  
        
        
        return self.matcher_patterns
    
    def patternExist(self, name):
        for i in self.matcher_patterns:
            if i[0]['NAME'] == name:
                return True
            
        return False    
    
    def createMatcherPattern(self):
        for i in self.matcher_patterns:
            self.matcher.add(i[0]['NAME'], [i[1]['PATTERN']])

        return self.matcher
    
    def analysisTweet(self, tweetList):
        for tweet in tweetList[:10]:
            doc = self.nlp(tweet)
        
        matches = self.matcher(doc)
        
        data = {}

        # Iterate over the matches and print the span text
        for match_id, start, end in matches:
            _type_match = self.nlp.vocab.strings[match_id].lower()
            
            if _type_match not in data:
                data[_type_match] = 1
            else:
                data[_type_match] = data[_type_match] + 1
